Introduction to Git

VCS/ Version Control System
	VCS is a piece of software that manages/ controls the revisions of a source code/ document/ collection of information. This allows the programmer/ developer to create versions.

	What is Git
		Git is an open source VCS that we use to track changes in a file within a directory/ repository.

		Repositories
			Local Repositories- Initialized folders that use git technology. Therefore, it allows us to track changes made in the files with the folder.  These changes can then be uploaded and updated in the remote repositories

			Remote Repositories- are folders that use git technology but instead of bing in local machine, they are located in the internet/cloud such as GitLaB and GitHub

		SSH Key
			Secure Shell Key - used to authenticate the uploading/ pushing or doing other tasks when manipulating or using git rpos. Passwords will not be required once SSH Keys are set.

		Basic Commands
			git init - initialize a new local git repository from common directories

			ssh-keygen - allows the devs to create ssh key for the device to be used in pushing to GitLab/ GitHub

			git remote add <alias> <gitSSHURL> - allows to connect our local repo to an online/remote repos. "origin" is the default alias for remote repositories.

			git remote remove <alias> - allows to delete an existing online/remote repo that is connected to the local repo